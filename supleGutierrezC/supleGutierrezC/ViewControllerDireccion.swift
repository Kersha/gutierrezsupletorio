//
//  ViewControllerDireccion.swift
//  supleGutierrezC
//
//  Created by carlos on 21/8/18.
//  Copyright © 2018 carlos. All rights reserved.
//

import UIKit

class ViewControllerDireccion: UIViewController {
    var direccion:Address?
    var id:String?
    let miembrosManager = MiembrosManager()
    
    @IBOutlet weak var txtDireccion: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        txtDireccion.text="\(String(describing: direccion?.addressLine1)),\(String(describing: direccion?.addressLine2)), \(String(describing: direccion?.city)), \(String(describing: direccion?.state)),"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clicEliminar(_ sender: Any) {
        
        miembrosManager.eliminarMiembro(id: id!)
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
