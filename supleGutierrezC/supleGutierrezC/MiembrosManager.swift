//
//  MiembrosManager.swift
//  supleGutierrezC
//
//  Created by carlos on 21/8/18.
//  Copyright © 2018 carlos. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

class MiembrosManager{
    
    var miembrosFiltrados:[Datum] = []
    var miembrosConsultados:[Datum] = []
    
    func obtenerMiembros(completion:@escaping(([Datum])->())){
        self.miembrosFiltrados = []
        self.miembrosConsultados = []
        
        let group = DispatchGroup()
        group .enter()
        Alamofire.request("https://private-534ec-amstronghuang.apiary-mock.com/members").responseJSON
            { response in
                guard let data = response.data else {
                    print("Error")
                    return
                }
                
                guard let miembrosConsultados = try? JSONDecoder().decode(MiembrosData.self, from: data) else {
                    print("Error decoding Evento")
                    return
                }
                self.miembrosConsultados=miembrosConsultados.data
                print(miembrosConsultados.data)
                
                completion(miembrosConsultados.data)
                let config = Realm.Configuration(
                    
                    schemaVersion: 1,
                    
                    migrationBlock: { migration, oldSchemaVersion in
                        
                        if (oldSchemaVersion < 1) {
                            
                        }
                })
                
                Realm.Configuration.defaultConfiguration = config
                
                 let realm = try! Realm()
                 let eliminados = realm.objects(MiembroId.self)
            
                
                print("eliminados",eliminados)
                
                    
                miembrosConsultados.data.forEach({ (miembro) in
                    var estaEliminado = false
                    eliminados.forEach({ (miembroEliminado) in
                        // group.enter()
                        if miembroEliminado.id == miembro.id {
                           
                            print("eliminado ",miembro.id)
                            estaEliminado=true
                        }
                        //group.leave()
                    })
                    
                    if !estaEliminado{
                        self.miembrosFiltrados.append(miembro)
                    }
                })
                
                
                //print (self.miembrosFiltrados)
                //
                group.leave()
                
        }
        
        group.notify(queue: .main){
            completion(self.miembrosFiltrados)
 
        }
    }
    
 
    
    func eliminarMiembro(id:String){
        let miembro = MiembroId()
        miembro.id=id
        
        let config = Realm.Configuration(
            
            schemaVersion: 1,
            
            migrationBlock: { migration, oldSchemaVersion in
                
                if (oldSchemaVersion < 1) {
                    
                }
        })
        
        Realm.Configuration.defaultConfiguration = config
        let realm = try! Realm()

        try! realm.write {
            realm.add(miembro)
            print("miembro eliminidad")
        }
        
    }
}
