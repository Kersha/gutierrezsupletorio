//
//  MiembrosData.swift
//  supleGutierrezC
//
//  Created by carlos on 21/8/18.
//  Copyright © 2018 carlos. All rights reserved.
//
//   let eventos = try? newJSONDecoder().decode(MiembrosData.self, from: jsonData)

import Foundation

struct MiembrosData: Codable {
    let data: [Datum]
}

struct Datum: Codable {
    let id, firstName, lastName, email: String
    let address:Address
    
}

struct Address: Codable {
    let id: Int
    let addressLine1, addressLine2, city, state: String
    let zipCode, createdAt, updatedAt: String
}


