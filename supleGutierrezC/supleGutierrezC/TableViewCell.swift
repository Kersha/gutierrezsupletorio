//
//  TableViewCell.swift
//  supleGutierrezC
//
//  Created by carlos on 21/8/18.
//  Copyright © 2018 carlos. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var txtEmail: UILabel!
    @IBOutlet weak var txtApellido: UILabel!
    @IBOutlet weak var txtNombre: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
