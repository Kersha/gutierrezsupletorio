//
//  ViewController.swift
//  supleGutierrezC
//
//  Created by carlos on 21/8/18.
//  Copyright © 2018 carlos. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var idSeleccionada:String = "algo"
    var direccionSeleccionada:Address?
    
    var datos:[Datum]=[]
    let miembrosManager = MiembrosManager()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datos.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "celda") as! TableViewCell
        
        cell.txtNombre.text = datos[indexPath.row].firstName
        cell.txtApellido.text = datos[indexPath.row].lastName
        cell.txtEmail.text = datos[indexPath.row].email
        
        return cell
    }
    
    func cargarDatos(){
        miembrosManager.obtenerMiembros(){
            (resultados)in
            self.datos = resultados
             //print(self.datos)
             self.tableView.reloadData()
        }
       
        
        
    
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        cargarDatos()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cargarDatos()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "direccion" {
            let destination = segue.destination as! ViewControllerDireccion
            destination.id = self.idSeleccionada
            destination.direccion = self.direccionSeleccionada
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.idSeleccionada = self.datos[indexPath.row].id
        self.direccionSeleccionada = self.datos[indexPath.row].address
            performSegue(withIdentifier: "direccion", sender: self)
    }

}

